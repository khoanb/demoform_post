// All material copyright ESRI, All Rights Reserved, unless otherwise specified.
// See http://gis1061.esrivn.net/portal/jsapi/jsapi/esri/copyright.txt and http://www.arcgis.com/apps/webappbuilder/copyright.txt for details.
//>>built
define(["dojo/_base/declare", "jimu/BaseWidget", "jimu/loaderplugins/jquery-loader!https://code.jquery.com/jquery-git1.min.js"], function (b, c,$) {
    return b([c], {
        
        baseClass: "jimu-widget-demo",
        postCreate: function ()
        {
            esriConfig.defaults.io.corsEnabledServers.push('192.168.1.142');
            this.inherited(arguments); console.log("postCreate");


            that1 = this;
        },
    // callAjax:   function uploadFile() {        
    //    var xhr = new XMLHttpRequest();                 
    //var file = document.getElementById('myfile').files[0];
    //xhr.open("POST", "http://192.168.1.142/api/Upload");
    //xhr.setRequestHeader("filename", file.name);
    //xhr.send(file);
    // },
     callAjax: function uploadFile() {
         var formData = new FormData();
         var file = $('#myfile')[0];
         formData.append('file', file.files[0]);
         $.ajax({  
             url: 'http://192.168.1.142/api/Upload',
             type: 'POST',  
             data: formData,  
             contentType: false,  
             processData: false,  
             success: function (d) {  
                 $('#updatePanelFile').addClass('alert-success').html('<strong>Upload Done!</strong><a href="' + d + '">Download File</a>').show();  
                 $('#file1').val(null);  
             },  
             error: function () {  
                 $('#updatePanelFile').addClass('alert-error').html('<strong>Failed!</strong> Please try again.').show();  
             }  
         });  
     },
        startup: function () { this.inherited(arguments); this.mapIdNode.innerHTML = "map id:" + this.map.id; console.log("startup") },
        onOpen: function () { console.log("onOpen") }, onClose: function () { console.log("onClose") },
        onMinimize: function () { console.log("onMinimize") }, onMaximize: function () { console.log("onMaximize") },
        onSignIn: function (a) { console.log("onSignIn") },
        onSignOut:function(){console.log("onSignOut")},showVertexCount:function(a){this.vertexCount.innerHTML="The vertex count is: "+a}})});